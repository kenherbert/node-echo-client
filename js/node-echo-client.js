/* global $ */


$('#send').click(() => {
    const message = $('#message').val();

    try {
        const body = {
            message: message,
            date: Date.now()
        };

        fetch('http://localhost:59595', {method:'POST', body: JSON.stringify(body)}).then((response) => {
            if(!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            return response.json();
        }).then((response) => {
            let date = new Date(response.date);

            $('#output').append(`<li class="list-group-item">${date.toLocaleString()}: ${response.message}</li>`);
        });
    } catch(err) {
        console.log(err);
    }
});


$('#message').keyup(evt => {
    if($(evt.target).val() === '') {
        $('#send').attr('disabled', 'true');
    } else {
        $('#send').removeAttr('disabled');
    }
});